describe Gitlab::Homepage::DevopsTool do
  subject(:devops_tool) { described_class.new(key, data) }
  let(:key) { 'gitlab_core' }
  let(:data) do
    {
      'name' => 'Core',
      'short_name' => 'Core',
      'logo' => 'logo.svg'
    }
  end

  describe '#gitlab?' do
    subject { devops_tool.gitlab? }

    it { is_expected.to be_truthy }

    context 'when key does not have gitlab prefix' do
      let(:key) { 'jira' }

      it { is_expected.to be_falsey }
    end
  end

  describe '#method_missing' do
    context 'name' do
      subject { devops_tool.name }

      it { is_expected.to eq('Core') }
    end

    context 'unknown' do
      subject { devops_tool.unknown }

      it { is_expected.to be_nil }
    end
  end

  describe '.all!' do
    subject { described_class.all! }

    before do
      described_class.instance_variable_set(:@features, nil)
      allow(YAML).to receive(:load_file) { { 'devops_tools' => { key => data } } }
    end

    it { expect(subject.count).to eq(1) }

    it 'returns DevopsTool objects' do
      devops_tool = subject.first

      expect(devops_tool).to be_kind_of(described_class)
    end
  end

  describe '.for_stage' do
    subject { described_class.for_stage(stage) }

    let(:stage) { instance_double('Gitlab::Homepage::Stage', categories: categories) }
    let(:categories) do
      [
        instance_double('Gitlab::Homepage::Category', key: 'wiki'),
        instance_double('Gitlab::Homepage::Category', key: 'pages')
      ]
    end

    before do
      described_class.instance_variable_set(:@features, nil)
      allow(YAML).to receive(:load_file) do
        {
          'devops_tools' => {
            'gitlab_core' => { 'name' => 'Core' },
            'gitlab_ultimate' => { 'name' => 'Ultimate', 'category' => ['wiki'] },
            'github' => { 'name' => 'Github', 'category' => ['wiki'] },
            'jira' => { 'name' => 'Jira' }
          }
        }
      end
    end

    it 'returns a devops tool (not from GitLab) with matching categories for stage' do
      devops_tools = subject

      expect(devops_tools.count).to eq(1)

      devops_tool = devops_tools.first

      expect(devops_tool).to have_attributes(key: 'github')
    end

    context 'when stage categories do not match' do
      let(:categories) do
        [
          instance_double('Gitlab::Homepage::Category', key: 'pages')
        ]
      end

      it { is_expected.to be_empty }
    end
  end
end
